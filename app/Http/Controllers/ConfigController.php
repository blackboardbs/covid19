<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigController extends Controller
{
    // public function store(Request $request)
    // {
    //     $user = Auth::user();



    //     return view('questions');
    // }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $organization = Organization::where('user_id', $user->id)->first();
        $config = Config::where('user_id', $user->id)->first();

        $config->text = $request->text;
        $config->background = $request->background;

        $config->save();

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'config' => $config,
            'organization' => $organization
        ];

        // return view('admin/customize')->with($parameters);

        return redirect('back')->with('Success', 'Data Updated Successfully!');
    }
}
