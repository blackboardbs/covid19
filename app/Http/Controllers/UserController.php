<?php

namespace App\Http\Controllers;

use App\Organization;
use Illuminate\Http\Request;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    public function member(Request $request)
    {
        $user = User::create([
            'name' => $request->email,
            'email' => $request->email,
            'password' => Hash::make(str_random(8)),
            'last_login' => Carbon::now(),
            'purchase_status' => 'trial'
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => 2,
        ]);

        return view('questions');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->userName;
        $user->email = strtolower(trim($request->userEmail));
        $user->contact_number = $request->contactNumber;
        $user->password = Hash::make($request->password);
        $user->last_login = Carbon::now();
        $user->purchase_status = 'trial';
        $user->save();

        if ($request->has('organization')){
            return response()->json(['user' => $user]);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout() {
        // dd('test');
        Auth::logout();
        return redirect()->back();
      }
}
