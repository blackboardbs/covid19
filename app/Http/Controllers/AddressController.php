<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index()
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $address = new Address();
        $address->address_line1 = $request->addressLine1;
        $address->address_line2 = $request->addressLine2;
        $address->suburb = $request->suburb;
        $address->city = $request->city;
        $address->province = $request->province;
        $address->country = $request->country;
        $address->zip_code = $request->zipCode;
        $address->save();

        return response()->json(['address' => $address]);
    }

    public function show()
    {

    }

    public function edit()
    {

    }

    public function update(Request $request, Address $address)
    {
        $address->address_line1 = $request->addressLine1;
        $address->address_line2 = $request->addressLine2;
        $address->suburb = $request->suburb;
        $address->city = $request->city;
        $address->province = $request->province;
        $address->country = $request->country;
        $address->zip_code = $request->zipCode;
        $address->save();

        return response()->json(['address' => $address]);
    }

    public function destroy()
    {

    }
}
