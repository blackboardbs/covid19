<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Organization;
use App\Config;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $organization = Organization::where('user_id', $user->id)->first();
        $config = Config::where('user_id', $user->id)->first();

        if (Carbon::parse($user->last_login)->subdays(14) >= $user->created_at && $user->purchase_status == 'trial') {
           
            // Auth::logout();
            return view('trial_ended')->with('user', $user);
        }

        $parameters = [
            'user' => $user,
            'organization' => $organization,
            'config' => $config
        ];

        return view('home')->with($parameters);
    }
}
