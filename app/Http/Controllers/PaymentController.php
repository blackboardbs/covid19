<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Billow\Contracts\PaymentProcessor;
use App\User;
use App\Order;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PaymentController extends Controller
{
    public function confirmPayment(PaymentProcessor $payfast)
    {
        // Eloqunet example.
        $cartTotal = 299.99;
        $order = Order::create([
            'm_payment_id' => Carbon::parse(now())->format('ymdHis'), // A unique reference for the order.
            'user_id' => Auth()->id(),
            'amount'       => $cartTotal,
        ]);

        $user = User::where('id', Auth()->id())->first();
        $organization = Organization::where('user_id', $user->id)->first();
//dd($user);
        // Build up payment Paramaters.
        $payfast->setBuyer($user->name, $user->name, $user->email);
        $payfast->setAmount($order->amount);
        $payfast->setItem('Covid App License', 'License for Covid App');
        $payfast->setMerchantReference($order->m_payment_id);

        // Return the payment form.
        $payform = $payfast->paymentForm('Place Order');

        return view('checkout_form', ['payform' => $payform ])->with('organization', $organization);
    }

    public function itn(Request $request, PaymentProcessor $payfast)
    {
        // Retrieve the Order from persistance. Eloquent Example.
        $order = Order::where('m_payment_id', $request->get('m_payment_id'))->firstOrFail(); // Eloquent Example

        // $order = new Order();
        // $order->amount = '300';
        // $oder->m_payment_id = $request->get('m_payment_id');
        // $order->save();

        $variables = $payfast->responseVars();
//
        // Verify the payment status.
        $status = $payfast->verify($request, $order->amount, $order->m_payment_id)->status();

        // Handle the result of the transaction.
        switch( $status )
        {
            case 'COMPLETE': // Things went as planned, update your order status and notify the customer/admins.
                $variables = $payfast->responseVars();
                $user = User::find($order->user_id);
                $user->purchase_status = 'purchased';
                $user->save();
                break;
            case 'FAILED': // We've got problems, notify admin and contact Payfast Support.
                break;
            case 'PENDING': // We've got problems, notify admin and contact Payfast Support.
                break;
            default: // We've got problems, notify admin to check logs.
                break;
        }
    }


    function generateSignature($data, $passPhrase = null) {
        // Create parameter string
        $pfOutput = '';
        foreach( $data as $key => $val ) {
            if(!empty($val)) {
                $pfOutput .= $key .'='. urlencode( trim( $val ) ) .'&';
            }
        }
        // Remove last ampersand
        $getString = substr( $pfOutput, 0, -1 );
        if( $passPhrase !== null ) {
            $getString .= '&passphrase='. urlencode( trim( $passPhrase ) );
        }
        return md5( $getString );
    }

    function paid() {

        $user = Auth::user();
        $organization = Organization::where('user_id', $user->id)->first();

        return view('paid')->with('organization', $organization);
    }

    function cancel() {

        $user = Auth::user();
        // $user->purchase_status = 'trial';
        // $user->save();

        // $organization = Organization::where('user_id', $user->id)->first();

        $merchant_id = 10013304;
        $version = 'v0.3.2';
        $timestamp = Carbon::now()->toDateTimeString();
        $passphrase = 'sand3bOxPass';
        $signature = Hash::make($merchant_id.$version.$timestamp.$passphrase);

        return redirect('https://api.payfast.co.za/'.$merchant_id.'-'.$version.'-'.$timestamp.'-'.$signature.'/cancel');
        // return view('cancel')->with('organization', $organization);
    }
}
