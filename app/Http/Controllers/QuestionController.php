<?php

namespace App\Http\Controllers;

use App\Form;
use App\GuestAnswers;
use App\Organization;
use App\Question;
use App\User;
use App\Visitor;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function home(Organization $organization)
    {
        return view('home')->with(['organization' => $organization]);
    }

    public function questions(Organization $organization)
    {
        $user = User::find(session('user_id'));

        $form_inputs = Form::with('dropDownItems')
            ->where('organization_id', $organization->id)
            ->orWhere('organization_id', 0)
            ->get();

        $questions = $form_inputs->map(function ($input) use($user, $organization){
            $value = Question::where('form_id', $input->id)
                //->where('organization_id', $organization->id)
                ->where('user_id', $user->id)->latest()->first();
            return [
                'id' => $input->id,
                'input_type' => $input->input_type,
                'value' => $value->value??null,
                'label' => $input->label,
                'dropdown_items' => $input->dropDownItems->map(function ($item){
                    return [
                        'id' => $item->id,
                        'value' => $item->drop_down_value,
                    ];
                })
            ];
        });

        $parameters = [
            'user' => $user,
            'questions' => $questions,
            'organization' => $organization
        ];

        return view('questions.index')->with($parameters);
    }

    public function answers(Request $request,Organization $organization)
    {
        $form_inputs = Form::where('organization_id', $organization->id)->get(['label', 'id', 'expected_answer']);
        $standard_inputs = Form::where('organization_id', 0)->get(['label', 'id', 'expected_answer']);

        if ($request->has('guest_name') && $request->guest_name != ''){
            $visitor = new Visitor();
            $visitor->user_id = session('user_id');
            $visitor->guests = 0;
            $visitor->guests_previous = 0;
            $visitor->guests_number = 0;
            $visitor->organization_id = $organization->id;
            $visitor->save();

            foreach ($form_inputs as $input) {
                $guest = new GuestAnswers();
                $guest->user_id = session('user_id');
                $guest->guest_name = $request->guest_name;
                $guest->form_id = $input->id;
                $guest->value = $request->input(strtolower(str_replace(' ', '_', $input->label)));
                $guest->visit_id = $visitor->id;

                if ($input->expected_answer != NULL) {
                    if ($request->input(strtolower(str_replace(' ', '_', $input->label))) != $input->expected_answer) {
                        return redirect()->route('denied', $organization);
                    }
                }
            }

        }else{
            foreach ($form_inputs as $input){
                $answer = new Question();
                $answer->form_id = $input->id;
                $answer->user_id = session('user_id');
                $answer->value = $request->input(strtolower(str_replace(' ', '_', $input->label)));
                //$answer->organization_id = $organization->id;
                $answer->save();

                if ($input->expected_answer != NULL) {
                    if ($request->input(strtolower(str_replace(' ', '_', $input->label))) != $input->expected_answer) {
                        return redirect()->route('denied', $organization);
                    }
                }
            }

        }

        if (session('guests_number')){
            $guests = session('guests_number');
            $guests_left = session('guests_number_left');
            if (!isset($guests_left)){
                $guests_left = $guests - 1;
            }else{
                $guests_left = session('guests_number_left') - 1;
            }
            $number = $guests - $guests_left;

            session(['guests_number_left' => $guests_left]);
            session(['guest_number' => $number]);
            if (session('guests_number_left') >= 0){
                return redirect()->route('questions', $organization)->with(['guest_number' => $number]);
            }else{
                $request->session()->forget('guests_number');
                $request->session()->forget('guests_number_left');
                $request->session()->forget('guest_number');
                return redirect()->route('exit', $organization);
            }

        }

        $request->session()->forget('guests_number');

        return redirect()->route('exit', $organization);
    }

    public function exit(Organization $organization)
    {
        request()->session()->forget('user_id');
        return view('questions.exit')->with(['organization' => $organization]);
    }

    public function denied(Organization $organization)
    {
        request()->session()->forget('user_id');
        return view('questions.denied')->with(['organization' => $organization]);
    }

}
