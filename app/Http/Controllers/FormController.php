<?php

namespace App\Http\Controllers;

use App\DropDownItem;
use App\Form;
use App\Organization;
use Illuminate\Http\Request;
use Auth;
use App\UserRole;

class FormController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Organization $organization)
    {
        $forms = Form::with(['organization', 'dropDownItems'])
            ->where('organization_id', $organization->id)
            ->paginate(15);

        $standard_forms = Form::with(['organization', 'dropDownItems'])
            ->where('organization_id', 0)
            ->paginate(15);

        $user_id = Auth::user()->id;
        $user_role = UserRole::where('user_id', $user_id)->first();

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'org_id' => $organization,
            'organization' => $organization,
            'form_inputs' => $forms,
            'standard_inputs' => $standard_forms
        ];

        return view('form.index')->with($parameters);
    }

    public function create()
    {
      return view('form.create');
    }

    public function store(Request $request, Form $form)
    {
        $form = new Form();
        $form->organization_id = $request->segment(2);
        $form->input_type = $request->inputType;
        $form->label = $request->label;
        $form->expected_answer = $request->expected_answer;
        $form->save();

        if (!empty($request->dropDownValues)) {

            foreach ($request->dropDownValues as $item){
                $drop_down_items = new DropDownItem();
                $drop_down_items->drop_down_value = $item;
                $drop_down_items->form_id = $form->id;
                $drop_down_items->save();
            }
        }

        return response()->json(['input' => $form->load('dropDownItems') ]);
    }

    public function show(Form $form)
    {

    }

    public function edit($id, $form_id)
    {
        return response()->json(['input' => Form::find(\request()->segment(3))]);
    }

    public function update(Form $form)
    {
        $form->input_type = \request()->inputType;
        $form->label = \request()->label;
        $form->save();

        return response()->json(['input' => $form]);
    }

    public function destroy(Form $form)
    {
        $form_id = $form->id;
        $form->delete();
        return response()->json(['form_id' => $form_id]);
    }
}
