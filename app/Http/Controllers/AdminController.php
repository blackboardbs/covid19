<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\UserRole;
use App\Config;
use App\Organization;
use App\Visitor;
use Illuminate\Support\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth()->user();
        if (Carbon::parse($user->last_login)->subdays(14) >= $user->created_at && $user->purchase_status == 'trial') {
           $organization = Organization::where('user_id', $user->id)->first();

            $parameters = [
                'user' => $user,
                'organization' => $organization
            ];

           return view('trial_ended')->with($parameters);
        }

        if (Carbon::parse($user->last_login)->subdays(10) >= $user->created_at && $user->purchase_status == 'trial') {
            try {
                Mail::to($user->email)->send(new ExpiringMail($user));
            } catch (\Exception $exception){
    
            }
        }

        $user_id = Auth::user()->id;
        $user_role = UserRole::where('user_id', $user_id)->first();
        $organization = Organization::where('user_id', $user_id)->first();

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'organization' => $organization
        ];

        return view('admin/index')->with($parameters);
    }

    public function customize()
    {
        $user_id = Auth::user()->id;
        $user_role = UserRole::where('user_id', $user_id)->first();
        $config = Config::where('user_id', $user_id)->first();
        $organization = Organization::where('user_id', $user_id)->first();

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'config' => $config,
            'organization' => $organization
        ];

        return view('admin/customize')->with($parameters);
    }

    public function customize_store(Request $request)
    {
        $user_id = Auth::user()->id;
        $user_role = UserRole::where('user_id', $user_id)->first();
        $config = Config::where('user_id', $user_id)->first();
        $organization = Organization::where('user_id', $user_id)->first();

        $config->text = $request->text;
        $config->background = $request->background;
        $config->menu = '';

        // if ($request->hasFile('logo')) {
        //     $request->file('logo')->store('public/organization');
        //     $image = Image::make($path.'/' . $request->file('logo')->hashName());
        //     if ($image->height() == $image->width()) {
        //         $image->resize(200, 200);
        //         $image->save();
        //     } else {
        //         $blur = Image::make($path.'/' . $request->file('logo')->hashName())->fit(200, 200)->blur();

        //         $image->resize(200, 200, function ($c) {
        //             $c->aspectRatio();
        //             $c->upsize();
        //         });

        //         $blur->insert($image, 'center');
        //         $blur->save();
        //     }

        //     $config->logo = $request->file('logo')->hashName();
        //     $organization->logo = $request->file('logo')->hashName();
        // };

        $config->save();
        // $organization->save();

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'config' => $config,
            'organization' => $organization
        ];

        return view('admin/customize')->with($parameters);
    }

    public function analytics()
    {
        $user_id = Auth::user()->id;
        $user_role = UserRole::where('user_id', $user_id)->first();
        
        $organization = Organization::where('user_id', $user_id)->first();
        $visitors = Visitor::where('organization_id', $organization->id)->get();

        $new_visitors = 0;
        foreach($visitors as $visitor){
            if ($visitor->created_at > Carbon::now()->subDays(13)) {
                $new_visitors = $new_visitors + 1;
            }
        };

        $returning_visitors = 0;
        foreach($visitors as $visitor){
            if ($visitor->visits > 1) {
                $returning_visitors = $returning_visitors + 1;
            }
        };

        $now = Carbon::now();
        $start = $now->startOfWeek(Carbon::MONDAY);
        $end = Carbon::parse($start)->addDays(7);

        // dd($end);

        $monday = 0;
        $tuesday = 0;
        $wednesday = 0;
        $thursday = 0;
        $friday = 0;
        $saturday = 0;
        $sunday = 0;
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Mon' && Carbon::parse($visitor->created_at) > $start && Carbon::parse($visitor->created_at) < $end) {
                $monday = $monday + 1;
            }
        };
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Tue' && Carbon::parse($visitor->created_at) > $start && Carbon::parse($visitor->created_at) < $end) {
                $tuesday = $tuesday + 1;
            }
        };
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Wed' && Carbon::parse($visitor->created_at) > $start && Carbon::parse($visitor->created_at) < $end) {
                $wednesday = $wednesday + 1;
            }
        };
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Thu' && Carbon::parse($visitor->created_at) > $start && Carbon::parse($visitor->created_at) < $end) {
                $thursday = $thursday + 1;
            }
        };
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Fri' && Carbon::parse($visitor->created_at) > $start ) {
                $friday = $friday + 1;
            }
        };
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Sat' && Carbon::parse($visitor->created_at) > $start && Carbon::parse($visitor->created_at) < $end) {
                $saturday = $saturday + 1;
            }
        };
        foreach($visitors as $visitor){
            if (Carbon::parse($visitor->created_at)->format('D') == 'Sun' && Carbon::parse($visitor->created_at) > $start && Carbon::parse($visitor->created_at) < $end) {
                $sunday = $sunday + 1;
            }
        };

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'organization' => $organization,
            'visitors' => $visitors,
            'new_visitors' => $new_visitors,
            'returning_visitors' => $returning_visitors,
            'monday' => $monday,
            'tuesday' => $tuesday,
            'wednesday' => $wednesday,
            'thursday' => $thursday,
            'friday' => $friday,
            'saturday' => $saturday,
            'sunday' => $sunday,
        ];

        return view('admin/analytics')->with($parameters);
    }

    public function barcode()
    {
        $user_id = Auth::user()->id;
        $user_role = UserRole::where('user_id', $user_id)->first();
        $organization = Organization::where('user_id', $user_id)->first();

        $parameters = [
            'user_id' => $user_id,
            'user_role' => $user_role,
            'organization' => $organization,
        ];

        return view('admin/barcode')->with($parameters);
    }

    public function download_qr($organization)
    {
        $organization = Organization::where('id', $organization)->first();

        return view('admin/download')->with('organization', $organization);
        
    }
}
