<?php

namespace App\Http\Controllers;

use App\Organization;
use App\User;
use App\Visitor;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    public function captureVisit(Request $request, Organization $organization)
    {
        $user = User::where('email', $request->email)->first();
        session()->forget('user_id');
        isset($user)?session(['user_id'=>$user->id]):'';

        $parameters = [
            'user' => $user,
            'organization' => $organization,
            'email' => $request->email,
        ];
        return view('visit.visit')->with($parameters);
    }

    public function store(Request $request, Organization $organization)
    {
        $user_id = session('user_id');

        $user = isset($user_id)?User::find($user_id):new User();
        $user->name = $request->visitor_name;
        $user->email = $request->email;
        $user->contact_number = $request->cellphone_number;
        $user->save();

        if (!isset($user_id)){
            session(['user_id' => $user->id]);
        }

        $visitor = new Visitor();
        $visitor->user_id = $user->id;
        $visitor->guests = $request->guests??0;
        $visitor->guests_previous = $request->guests_previous??0;
        $visitor->guests_number = $request->guests_number??0;
        $visitor->organization_id = $request->organization_id;
        $visitor->visits = $visitor->visits + 1;
        $visitor->save();

        session(['visitor_id' => $visitor->id]);

        if ($request->has('guests_number') && $request->guests_number > 0){
            session(['guests_number' => $request->guests_number]);
        }

        return redirect()->route('questions', $organization);
    }
}
