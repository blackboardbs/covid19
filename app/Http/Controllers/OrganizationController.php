<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeOrganization;
use App\Mail\CancelMail;
use App\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use App\Config;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class OrganizationController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except' => ['create', 'store', 'show', 'welcome']]);
    }

    public function index()
    {
        $organisations = Organization::where('user_id', auth()->id())->get();

        return view('organization.index')->with(['organizations' => $organisations]);

    }

    public function create()
    {
        $organization = Organization::all();
        $organization_count = $organization->count();

        $users = User::all()->pluck('email');

        $parameters = [
            'organization_count' => $organization_count,
            'users' => $users
        ];

        return view('organization.create')->with($parameters);
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'registration' => 'max:255',
            'vat_number' => 'max:255',
            'user_id' => 'required',
            'address_id' => 'nullable',
            'welcome_message' => 'max:255',
            'logo' => 'image|mimes:jpeg,png,jpg',
            'exit_link' => 'max:255',
            'exit_link_text' => 'max:30',
            'account_active' => 'nullable',
            'terms_accepted' => 'nullable',
        ]);

        $path = storage_path('app/public/organization');
        $organization = new Organization();
        $organization->name = $request->name;
        $organization->registration_number = $request->registration;
        $organization->vat_number = $request->vat_number;
        $organization->user_id = $request->user_id;
        $organization->address_id = $request->address_id;
        $organization->welcome_message = $request->welcome_message;
        $organization->exit_link = $request->exit_link;
        $organization->exit_link_label = $request->exit_link_text;
        $organization->account_active = 1;
        $organization->terms_accepted = now();
        $organization->ip_address = $request->ip();

        if ($request->hasFile('logo')) {
            $request->file('logo')->store('public/organization');
            $image = Image::make($path.'/' . $request->file('logo')->hashName());
            if ($image->height() == $image->width()) {
                $image->resize(350, 200);
                $image->save();
            } else {
                // $blur = Image::make($path.'/' . $request->file('logo')->hashName())->fit(200, 200);

                // $image->resize(200, 200, function ($c) {
                //     $c->aspectRatio();
                //     $c->upsize();
                // });

                // $blur->insert($image, 'center');
                // $blur->save();
                $image->resize(350, 200);
                $image->save();
            }

            $organization->logo = $request->file('logo')->hashName();
        }

        $organization->save();

        $config = new Config();
        $config->user_id = $request->user_id;
        $config->office_id = $organization->id;
        $config->logo = '';
        $config->text = 'rgb(0,0,0)';
        $config->background = 'rgb(255,255,255)';
        $config->menu = '';

        $config->save();

        try {
            Mail::to($organization->user->email)->send(new WelcomeOrganization($organization));
        } catch (\Exception $exception){

        }



        // return redirect()->route('organization.show', $organization);
        return redirect()->route('organization.activate', $organization);
    }

    public function show(Organization $organization)
    {
        $date = Carbon::parse(now())->format('ymdHis');
        $merchant = '10013304';
        $version = 'v1';

        $data = [
            'merchant-id' => $merchant,
            'version' => $version,
            'timestamp' => $date,
        ];

        $signature = app('App\Http\Controllers\PaymentController')->generateSignature($data);

        return view('organization.show')->with(['organization' => $organization->load('user', 'address')])->with('date', $date)->with('signature', $signature);
    }

    public function edit(Organization $organization)
    {
        return view('organization.edit')->with(['organization' => $organization]);
    }

    public function update(Request $request, Organization $organization)
    {
        $path = storage_path('app/public/organization');

        $organization->name = $request->name;
        $organization->registration_number = $request->registration;
        $organization->vat_number = $request->vat_number;
        $organization->user_id = $request->user_id;
        $organization->address_id = $request->address_id;
        $organization->welcome_message = $request->welcome_message;
        $organization->exit_link = $request->exit_link;
        $organization->exit_link_label = $request->exit_link_text;
        $organization->account_active = 0;
        //$organization->terms_accepted = now();
        $organization->ip_address = $request->ip();

        if ($request->hasFile('logo')) {
            $request->file('logo')->store('public/organization');
            $image = Image::make($path.'/' . $request->file('logo')->hashName());
            if ($image->height() == $image->width()) {
                $image->resize(350, 200);
                $image->save();
            } else {
                // $blur = Image::make($path.'/' . $request->file('logo')->hashName())->fit(200, 200)->blur();

                // $image->resize(200, 200, function ($c) {
                //     $c->aspectRatio();
                //     $c->upsize();
                // });

                // $blur->insert($image, 'center');
                // $blur->save();

                $image->resize(350, 200);
                $image->save();
            }

            $organization->logo = $request->file('logo')->hashName();
        }

        $organization->save();

        return redirect()->route('organization.show', $organization);
    }


    public function destroy(Organization $organization)
    {
        $organization->delete();

        return redirect()->back();
    }

    public function welcome(Organization $organization)
    {
        $organization->account_active = 1;
        $organization->save();

        return view('organization.account-activated')->with(['organization' => $organization]);
    }

    function cancel() {

        $user = Auth::user();
        $user->purchase_status = 'trial';
        $user->save();

        $organization = Organization::where('user_id', $user->id)->first();

        try {
            Mail::to('support@blackboardbs.com')->send(new CancelMail($user));
        } catch (\Exception $exception){

        }

        // $merchant_id = 10013304;
        // $version = 'v0.3.2';
        // $timestamp = Carbon::now()->toDateTimeString();
        // $passphrase = 'sand3bOxPass';
        // $signature = Hash::make($merchant_id.$version.$timestamp.$passphrase);

        // return redirect('https://api.payfast.co.za/'.$merchant_id.'-'.$version.'-'.$timestamp.'-'.$signature.'/cancel');
        return view('cancel')->with('organization', $organization);
    }
}
