<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    use softDeletes;

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public function dropDownItems()
    {
        return $this->hasMany(DropDownItem::class, 'form_id');
    }
}
