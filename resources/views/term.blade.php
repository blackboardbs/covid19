@extends('layouts.app')

@section('content')


<div class="login-wrap">
    	<div class="login-html">
    
            <img class="img" src="https://covid-19.blackboardbs.com/assets/logo.jpg" alt="">
            
            <label class="label"><h3>Blackboard Business Solutions Terms and Conditions</h3></label>
            
            <div class="container">
                
                <label class="label"><h5>Terms and Conditions</h5></label>
               
                <p>
                    <ol>
                        <li>Using our Services</li>
                        <li>Privacy and Copyright Protection</li>
                        <li>The client Content in our Services</li>
                        <li>About Software in our Services</li>
                        <li>Modifying and Terminating our Services</li>
                        <li>Business uses of our Services</li>
                        <li>About these Terms</li>
                        <li>Key aspects of Maintenance and support</li>
                        <li>9	Dependencies</li>
                        <li>Purchase includes</li>
                        <li>Assumptions</li>
                        <li>Term</li>
                        <li>General</li>
                        <li>Charges</li>
                        <li>Payments</li>
                        <li>Warranties</li>
                        <li>Limitations and exclusions of liability</li>
                        <li>Termination</li>
                        <li>Effects of termination</li>
                    </ol>
                </p><br>
                <hr>
                <p>
                    <b>1	Using our Services</b><br>
                    The client must follow any policies made available to the client within the Services provided by Blackboard Business Solutions. 
                    The client may not utilise Blackboard Business Solution services for any illegal or harmful intent. The system may not be accessed other than the interface and the instructions that Blackboard Business Solutions provide. The client may use our Services only as permitted by law. Blackboard Business Solutions may suspend or stop providing Services to the client if the client do not comply with our terms or policies or if Blackboard Business Solutions  are investigating suspected misconduct.
                    Using our Services does not give the client ownership of any intellectual property rights in our Services or the content. The client may not use content from our Services unless the client obtain permission from its owner or are otherwise permitted by law. These terms do not grant the client the right to use any branding or logos used in our Services. Don’t remove, obscure, or alter any legal notices displayed in or along with our Services.
                    Blackboard Business Solutions may review content to determine whether it is illegal or violates our policies, and Blackboard Business Solutions may remove or refuse to display content that Blackboard Business Solutions  reasonably believe violates our policies or the law. This however does not imply that Blackboard Business Solutions review content. 
                    In connection with the client use of the Services, Blackboard Business Solutions may send the client service announcements, administrative messages, and other information. The client may opt out of some of those communications.
                    Some of our Services are available on mobile devices. Do not use such Services in a way that distracts the client and prevents the client from obeying traffic or safety laws.<br>
                    <br><b>2	Privacy and Copyright Protection</b><br>
                    Blackboard Business Solutions respond to notices of alleged copyright infringement and terminate accounts of repeat infringers according to the process set out in this agreement. 
                    Nothing in this Agreement shall operate to assign or transfer any Intellectual Property Rights from the Licensor to the Licensee, or from the Licensee to the Licensor.<br>
                    <br><b>3	The client Content in our Services</b><br>
                    Some of our Services allow the client to upload, submit, store, send or receive content. The client retains ownership of any intellectual property rights that the client hold in that content. <br>
                    <br><b>4	About Software in our Services</b><br>
                    When a Service requires or includes downloadable software, this software may update automatically on the client device once a new version or feature is available. Some Services may let the client adjust the client automatic update settings.<br>
                    <br><b>5	General</b><br>
                    <ol type="a">
                        <li>Each party acknowledges that it has read this Agreement, they understand the agreement and agree to be bound by its terms. Further, both parties agree that this is the complete and exclusive statement of the Agreement between Blackboard Business Solutions and the parties, which supersedes and merges all prior proposals, understandings and all other agreements, oral and written, Blackboard Business Solutions and the parties relating to this Agreement. This Agreement may not be modified or altered except by written duly executed by both parties. The Software and the use thereof are subject to the license agreement related to the Software.</li>
                        <li>If any provision of this Agreement is invalid under any applicable statute or rule of law, it is to that extent, deemed to be omitted.</li>
                        <li>Client may not assign or sub-license without the prior written consent of Blackboard Business Solutions.</li>
                        <li>Support technicians of Blackboard Business Solutions will have access to client data, however, this will be for supporting purposes only within the client`s realm and ringfenced for client purposes. All backups of data will stake take place within the chosen service provider (in this case Amazon Web Services) and it will be incumbent on Blackboard Business Solutions to only keep client data within the chosen service provider and for purposes agreed to by the client.  </li>
                    </ol>
                    <br><b>6	Charges</b><br>
            
                    <br><b>7	Modifying and Terminating our Services</b><br>
                    Blackboard Business Solutions are constantly changing and improving Services. Blackboard Business Solutions may add or remove functionalities or features, and Blackboard Business Solutions may suspend or stop a Service altogether, however the client will be notified 30 days in advance. <br>
                    <br><b>8	Business uses of our Services</b><br>
                    If the client is using our Services on behalf of a business, that business accepts these terms. It will hold harmless and indemnify Blackboard Business Solutions and its affiliates, officers, agents, and employees from any claim, suit or action arising from or related to the use of the Services or violation of these terms, including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys’ fees.<br>
                    <br><b>9	About these Terms</b><br>
                    Blackboard Business Solutions may modify these terms or any additional terms that apply to a Service to, for example, reflect changes to the law or changes to our Services. The client should look at the terms regularly. Blackboard Business Solutions will post notice of modifications to these terms. 
                    These terms control the relationship between Blackboard Business Solutions and the client. They do not create any third-party beneficiary rights that the client undertakes. 
                    If the client do not comply with these terms, and Blackboard Business Solutions do not take action right away, this doesn’t mean that Blackboard Business Solutions are giving up any rights that Blackboard Business Solutions  may have (such as taking action in the future).<br>
                    <br><b>10	Key aspects of Maintenance and support</b><br>
                    <ul>
                        <li>The licensing fee will include all bug fixes identified by Blackboard Business Solutions and the client. </li>
                        <li>Updates and enhancements to the system will be made available to the client as part of the research and development performed by Blackboard Business Solutions. Enhancements suggestions will be considered as part of the research and development process. Enhancements outside standard research and development of the solution will be specified and costed separately. </li>
                    </ul>
                    <br><b>11	Dependencies</b><br>
                    Dependencies to the final delivery and hand over of the system will include the following:
                    <ul>
                        <li>Licensing agreement sign off. </li>
                        <li>System licensing.</li>
                    </ul>
                    <br><b>12	Purchase includes</b><br>
                    The license fee of the system will include the following:
                    <ul>
                        <li>System license </li>
                        <li>Access to online and Blackboard Business Solutions training </li>
                        <li>Access to online training material </li>
                    A Quotation will be provided for on site training should this be requested. 
                    </ul>
                    <br><b>13	Assumptions</b><br>
                    <ul>
                    <li>The solution remains the intellectual property of Blackboard Business Solutions.</li>
                    </ul>
                    <br><b>14	Term</b><br>
                    This agreement shall start on the Effective Date stated below or when registering by the client to this service. 
                    Payment for each renewal term shall be due on the renewal date - monthly - at the current rates for support of the Software. This agreement may be terminated for non-payment or material breach. Fees paid or due are non-refundable unless Blackboard Business Solutions has materially breached this agreement and has failed to cure the breach after 30 days written notice.
                    <ol>
                        <li type="a">The Licensee shall pay the Charges to the Licensor in accordance with this Agreement.</li>
                        <li type="a">All amounts stated in or in relation to this Agreement are, unless the context requires otherwise, stated inclusive of any applicable value added taxes. </li>
                    </ol>
                    <br><b>15	Payments</b><br>
                    <ol>
                        <li type="a">The Licensor shall issue invoices for the Charges to the Licensee</li>
                        <li type="a">The Licensee must pay the Charges to the Licensor within the period of 7 days following the issue of an invoice in. </li>
                        <li type="a">The Licensee must pay the Charges by direct debit or bank transfer. </li>
                        <li type="a">If the Licensee does not pay any amount properly due to the Licensor under this Agreement, the Licensor may cancel this agreement and terminate services. </li>
                    </ol>
                    <br><b>16	Warranties</b><br>
                    <ol>
                        <li type="a">The Licensor warrants to the Licensee that it has the legal right and authority to enter into this Agreement and to perform its obligations under this Agreement.</li>
                        <li type="a">The Licensor warrants to the Licensee that:</li>
                        <li type="a">the Software as provided will conform in all [material] respects with the Software Specification;</li>
                        <li type="a">The Software will be supplied free from Software Defects and will remain free from Software Defects within reason. </li>
                        <li type="a">The Software will be supplied free from viruses, worms, Trojan horses, ransomware, spyware, adware and other malicious software programs. Blackboard Business Solutions are indemnified from malicious hacking of the application or data outside of the precautions taken as herein mentioned. </li>
                        <li type="a">The Software shall incorporate security features reflecting the requirements of good industry practice. </li>
                        <li type="a">The Licensor warrants to the Licensee that the Software (when used by the Licensee in accordance with this Agreement) will not breach any laws, statutes or regulations applicable.</li>
                        <li type="a">If the Licensor reasonably determines, or any third party alleges, that the use of the Software by the Licensee in accordance with this Agreement infringes any person's Intellectual Property Rights, the Licensor may (acting reasonably) at its own cost and expense:</li>
                            <ol>
                            <li type="a">Modify the Software in such a way that it no longer infringes the relevant Intellectual Property Rights, providing that any such modification must not introduce any Software Defects into the Software and must not result in the Software failing to conform with the Software Specification.
                            </ol>
                        <li type="a">Blackboard Business Solutions will undertake all reasonable efforts to provide technical assistance under this agreement and to rectify or provide solutions to problems where the Software does not function as intended.</li>
                        <li type="a">Even though Blackboard Business Solutions takes all precautionary measures and complies to international data regulation standards, Blackboard Business Solutions is not liable for incidental, special or consequential damages for any reason (including loss of data or other business or property damage), even if foreseeable or if the client has advised of such a claim. Blackboard Business Solutions’ liability shall not exceed the fees that Client has paid under this agreement. </li>
                    <br><b>17	Limitations and exclusions of liability</b><br>
                    <ol>
                        <li type="a">Nothing in this Agreement will:</li>
            
                            <ol>
                                <li type="a">Hold Blackboard Business Solutions (the licensor) liable for death or personal injury resulting from negligence;</li>
                                <li type="a">Include any liability for fraud or fraudulent misrepresentation;</li>
                                <li type="a">Include any liabilities in any way that is not permitted under applicable law; or</li>
                                <li type="a">Include any liabilities that may not be excluded under applicable law.</li>
                            </ol>
                        <li type="a">The Licensor shall not be liable to the Licensee or The Licensee shall not be liable to the Licensor in respect of any loss of profits or anticipated savings.</li>
                        <li type="a">Neither party shall be liable to the other party in respect of any loss of revenue or income.</li>
                        <li type="a">Neither party shall be liable to the other party in respect of any loss of business, contracts or opportunities.</li>
                        <li type="a">Neither party shall be liable to the other party in respect of any loss or corruption of any data, database or software unless in cases where gross negligence was evident. </li>
                        <li type="a">Neither party shall be liable to the other party in respect of any special, indirect or consequential loss or damage.</li>
                    </ol>
                    <br><b>18	Termination</b><br>
                    <ol>
                        <li type="a">The Licensor may terminate this Agreement by giving to the Licensee [not less than 30 days'] written notice of termination</li>
                        <li type="a">The Licensee may terminate this Agreement by giving to the Licensor [not less than 30 days'] written notice of termination. Termination of this agreement will result in access to the application being revoked by Blackboard Business Solutions and any data will be considered that of the licensee and provided to the licensee accordingly. </li>
                        <li type="a">Either party may terminate this Agreement immediately by giving written notice of termination to the other party if:</li>
                        <li type="a">The other party commits any breach of this Agreement, and the breach is not remediable;</li>
                        <li type="a">The other party commits a breach of this Agreement, and the breach is remediable, but the other party fails to remedy the breach within the period of 30 days following the giving of a written notice to the other party requiring the breach to be remedied.</li>
                        <li type="a">Either party may terminate this Agreement immediately by giving written notice of termination to the other party if the other party:</li>
                            <ol>
                                <li type="a">is dissolved;</li>
                                <li type="a">ceases to conduct all (or substantially all) of its business;</li>
                                <li type="a">is or becomes unable to pay its debts as they fall due;</li>
                                <li type="a">is or becomes insolvent or is declared insolvent; or</li>
                                <li type="a">convenes a meeting or makes or proposes to make any arrangement or composition with its creditors;</li>
                                <li type="a">an administrator, administrative receiver, liquidator, receiver, trustee, manager or similar is appointed over any of the assets of the other party;</li>
                            </ol>
                    </ol>
                    <br><b>19	Effects of termination</b><br>
                    <ol>
                        <li type="a">Upon the termination of this Agreement, all of the provisions of this Agreement shall cease to have effect, save that the following provisions of this Agreement shall survive and continue to have effect (in accordance with their express terms or otherwise indefinitely)</li>
                        <li type="a">Except to the extent that this Agreement expressly provides otherwise, the termination of this Agreement shall not affect the accrued rights of either party.
                        <li type="a">For the avoidance of doubt, the licenses of the Software in this Agreement shall terminate upon the termination of this Agreement; and, accordingly, the Licensee must immediately cease to use the Software upon the termination of this Agreement should the software be hosted with a service provider of choice by the licensee (the client). In the case of hosting by Blackboard Business Solutions, access to the domain will be revoked upon termination of this agreement however data will be transferred to domain and server of choice and all access to this data by Blackboard Business Solutions will be revoked.  </li>
                        <li type="a">Within 10 Business Days following the termination of this Agreement, the Licensee shall:</li>
                            <ol>
                                <li type="a">	return to the Licensor or dispose of as the Licensor may instruct all media in its possession or control containing the Software or related to the software; and</li>
                                <li type="a">b.	irrevocably delete from all computer systems in its possession or control all copies of the Software should the software have been obtained in downloaded format in any way </li>
                            </ol>
                    </ol>
            
                </p>
            
           
            
            {{-- <button type="submit" class="button">Continue</button> --}}

        </div>
            
           
    		
    	
    	</div>
    </div>

@endsection


   
{{-- extra css --}}

<style>
    .button{
    	border:none;
    	padding:15px 20px;
    	border-radius:25px;
    	background:blue;
        width:100%;
    	color:#fff;
        text-transform:uppercase;
       
    }
    
    .button:hover{
        background-color: grey;
        }
    
    .input{
    	border:none;
    	padding:15px 20px;
    	border-radius:25px;
    	background:whitesmoke;
        width:100%;
    	color:#fff;
        
    	
    }
    
    .label{
    	color:whitesmoke;
    	font-size:20px;
    }
    
    .img{
        
    	margin:auto;
        max-width:150px;
    	min-height:50px;
    	position:relative;
        display: block;
        padding-bottom: 15px;
    }
    body{
    	margin:0;
    	color:#6a6f8c;
    	background:#c8c8c8;
    	font:600 16px/18px 'Open Sans',sans-serif;
    }
    *,:after,:before{box-sizing:border-box}
    .clearfix:after,.clearfix:before{content:'';display:table}
    .clearfix:after{clear:both;display:block}
    a{color:inherit;text-decoration:none}
    
    .login-wrap{
    	width:100%;
    	margin:auto;
    	max-width:525px;
    	min-height:670px;
    	position:relative;
    	background:url(https://raw.githubusercontent.com/khadkamhn/day-01-login-form/master/img/bg.jpg) no-repeat center;
    	box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
    }
    .login-html{
    	width:100%;
    	height:100%;
    	position:absolute;
    	padding:90px 70px 50px 70px;
    	background:rgba(40,57,101,.9);
    }
    
    </style>

    
    
