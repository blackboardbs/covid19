@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="text-center">
            <div class="alert alert-danger">
                Your trial period of 14 days has ended or your subscription has been cancelled.
                <br>
                Please renew your access by purchasing a license from the link below.
            </div>

        <p><a href="{{ route('pay_page', $user->id) }}"><img src="https://www.payfast.co.za/images/buttons/dark-large-subscribe.png" width="210" height="59" alt="Subscribe" title="Subscribe Now with PayFast" /></a></p>
        {{-- <a href="https://www.payfast.co.za/eng/process?cmd=_paynow&amp;receiver=14312122&amp;item_name=Covid+App+License&amp;item_description=License+for+covid+app+by+Blackboard&amp;amount=05.99&amp;return_url=https%3A%2F%2Fcovida.blackboardbs.com%2Fpaid&amp;cancel_url=https%3A%2F%2Fcovida.blackboardbs.com%2Fcancel&amp;cycles=0&amp;frequency=3&amp;m_payment_id=pay_now_14312122&amp;subscription_type=1"><img src="https://www.payfast.co.za/images/buttons/dark-large-subscribe.png" width="210" height="59" alt="Subscribe" title="Subscribe Now with PayFast" /></a> --}}
        </div>
    </div>

@endsection