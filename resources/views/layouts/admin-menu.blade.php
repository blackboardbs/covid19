@auth ()

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4" style="width: 12%; max-height: 300px;">

        <!-- Sidebar -->
        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <h4 style="color: #a9a9a9;"><strong>Admin Menu</strong></h4>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
                    <li class="nav-item">
                        <a href="{{ route('organization.show', $organization) }}" class="nav-link menu-item">
                        <i class="nav-icon fas fa-user"></i>
                            Profile
                        </a>                        
                    </li>
                    <li class="nav-item">
                        <a href="/forms/1" class="nav-link menu-item">
                        <i class="nav-icon fas fa-question"></i>
                            Add Questions
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.customize') }}" class="nav-link menu-item">
                        <i class="nav-icon fas fa-cog"></i>
                            Customize
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.analytics') }}" class="nav-link menu-item">
                        <i class="nav-icon fas fa-chart-line"></i>
                            Analytics
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.barcode') }}" class="nav-link menu-item">
                        <i class="nav-icon fas fa-qrcode"></i>
                            QR Code
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
@endauth
