<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <style>
        .sidebar-dark-primary {
            background-color: #656565 !important;
        }
        .sidebar .header, .sidebar-dark-primary .sidebar a {
            color: whitesmoke !important;
        }
        .sidebar .header, [class*=sidebar-dark] .user-panel, [class*=sidebar-dark] .brand-link {
            border-bottom: 1px solid #d4d4d4;
            border-top: 1px solid #d4d4d4;
        }
        .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active, .sidebar-dark-primary .nav-treeview>.nav-item>.nav-link.active:hover, .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active {
            background-color: #a9a9a9;
        }
    </style>

    @yield('extra-css')
</head>
<body onload="changeBody()">
    <div id="app">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-dark " style="background-color:#656565;">
            <div class="container">
            @isset($organization)
                <a href="{{route('enter', $organization)}}" class="navbar-brand">
                    @auth
                    <img src="{{asset('storage/organization/'.$organization->logo)}}" alt="" class="brand-image img-circle elevation-3"
                        style="opacity: .8">
                    @endauth
                    <span class="brand-text font-weight-light">Covid Screening</span>
                </a>
            @else 
                <a href="#" class="navbar-brand">
                    @auth
                    <img src="{!! asset('storage/logos/bbs_logo.jpeg') !!}" alt="" class="brand-image img-circle elevation-3"
                        style="opacity: .8">
                    @endauth
                    <span class="brand-text font-weight-light">Covid Screening</span>
                </a>
            @endauth
            <!-- Left navbar links -->
            @if(isset($organization))
                <ul class="navbar-nav">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('enter', $organization)}}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('admin.index') }}" class="nav-link">Admin</a>
                    </li>
                </ul>
            @else 
                <ul class="navbar-nav">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('admin.index') }}" class="nav-link">Admin</a>
                    </li>
                </ul>
            @endauth
            <!-- SEARCH FORM -->
            {{-- <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                    </button>
                </div>
                </div>
            </form> --}}
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                @auth ()
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-item" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    {{-- <img src="{!! asset('storage/logos/u.png') !!}" class="user-image" alt="" style="width: 15%; border-radius:50%;"> --}}
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs">{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        {{-- <img src="{!! asset('storage/logos/u.png') !!}" class="img-circle" alt="" style="max-width: 20% !important; border-radius:50%;"> --}}
                        <p>
                        {{auth()->user()->name}}
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="pull-left">
                            @auth
                            @if (isset($organization))
                            <a href="{{ route('organization.show', $organization) }}" class="btn btn-default btn-flat">Profile</a>
                            @endif
                            @endauth
                        </div>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                        <a href="{{ route('admin.index') }}" class="btn btn-default btn-flat">Admin Menu</a>
                        </div>
                        <div class="pull-right">
                        <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                    </ul>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('user.logout') }}" class="nav-link">Logout</a>
                </li>
                @else
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('login') }}" class="nav-link">Login</a>
                </li>
                @endauth
            </ul>
            </div>
        </nav>
        <!-- /.navbar -->

        <main class="py-4">
            @yield('content')
        </main>
        @auth
        @foreach (App\Config::all() as $config)
            @if ($config->user_id == auth()->user()->id)
            <div hidden>
                {{$text_color = $config->text}}
                <input type="text" id="text_color" name="text_color" value="{{$text_color}}">
                {{$background_color = $config->background}}
                <input type="text" id="background_color" name="background_color" value="{{$background_color}}">
            </div>
            @endif
        @endforeach
        @endauth

        <div style="margin-bottom: 4%;"></div>
        <div style="position: fixed; bottom: 0%; right: 1%;">Copyright © 2020 www.blackboardbs.com. All rights reserved.</div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    {{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.3.3/jscolor.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios@0.20.0/dist/axios.min.js" integrity="sha256-KyY5SqyBmXeM0zfYBGU1tuqcstxpjkECApypY+CA4Z8=" crossorigin="anonymous"></script>
    <script>
        // $( document ).ready(function() {
        function changeBody() {
            var text_color = $('#text_color').val();
            var background_color = $('#background_color').val();

            // console.log(text_color);

            document.body.style.color = text_color;
            document.body.style.background  = background_color;
        };
        // });
    </script>
    @yield('extra-js')
</body>
</html>


{{-- extra css --}}

<style>
body{
    	margin:0;
    	color:rgb(0,0,0);
    	background: rgb(255,255,255);
    	font:600 16px/18px 'Open Sans',sans-serif;
    }

</style>
