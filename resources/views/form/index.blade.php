@extends('layouts.app')

@section('content')

<div class="container-fluid" style="margin-top: 2%; display: inline-block;">
    <div class="row">
        @include('layouts.admin-menu')
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="border-style: groove; padding:2%; margin:2%;">
            <h3><strong>Screening Questions</strong></h3>
            <span>Customize your visitor questions here.</span>
            <div class="custom" style="margin-top: 2%;">
    <div class="text-center" style="margin-bottom: 2%;">
        <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#exampleModal" id="modal">
            Add Question or Field
        </button>
        <a href="{{route('enter', $org_id)}}" class="btn btn-info">Visitors Screen</a>
    </div>
    <table class="table table-sm table-striped">
        <thead>
        <tr>
            <th scope="col">Input Type</th>
            <th scope="col">Question/field label description</th>
            <th scope="col">Drop Down Values</th>
            <th scope="col">Expected Answer</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse($standard_inputs as $input)
                <tr id="{{$input->id}}">
                    <td>{{$input->input_type}}</td>
                    <td>{{$input->label}}</td>
                    <th>
                        @foreach($input->dropDownItems as $drop_down_item)
                            <span class="badge badge-primary mr-1">{{$drop_down_item->drop_down_value??null}}</span>
                        @endforeach
                    </th>
                    <td>{{$input->expected_answer}}</td>
                    <td>
                        <button class="btn btn-sm btn-warning edit" data-id="{{$input->id}}">Edit</button>
                        <button class="btn btn-sm btn-danger delete" data-id="{{$input->id}}">Delete</button>
                    </td>
                </tr>
                @empty
            @endforelse
            @forelse($form_inputs as $input)
                <tr id="{{$input->id}}">
                    <td>{{$input->input_type}}</td>
                    <td>{{$input->label}}</td>
                    <th>
                        @foreach($input->dropDownItems as $drop_down_item)
                            <span class="badge badge-primary mr-1">{{$drop_down_item->drop_down_value??null}}</span>
                        @endforeach
                    </th>
                    <td>{{$input->expected_answer}}</td>
                    <td>
                        <button class="btn btn-sm btn-warning edit" data-id="{{$input->id}}">Edit</button>
                        <button class="btn btn-sm btn-danger delete" data-id="{{$input->id}}">Delete</button>
                    </td>
                </tr>
                @empty
            @endforelse
        </tbody>
    </table>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Form input</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="input-type">Input Type</label>
                        <select class="form-control" id="input-type">
                            <option value="0">Select input type...</option>
                            <option value="text">Text (string)</option>
                            <option value="select">Drop down</option>
                            {{--<option value="radio">Radio Buttons</option>
                            <option value="date">Date</option>--}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="label">Question/field label description</label>
                        <input type="text" class="form-control" id="label">
                    </div>
                    <div class="form-group" id="dropdown-items" style="display: none">
                        <label for="drop-down-value">Drop down values</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <input type="text" class="form-control" id="drop-down-value">
                            <div class="input-group-append" style="cursor: pointer" id="add-possible-answer">
                                <div class="input-group-text"><strong>+</strong></div>
                            </div>
                        </div>
                    </div>
                    <div id="drop-vals"></div>
                    <div class="form-group">
                        <label for="label">Expected Answer</label>
                        <small style="display: block">Leave blank if any answer can be accepted.</small>
                        <input type="text" class="form-control" id="expected_answer">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-input">Add Question or Field</button>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
</div>
@endsection

@section('extra-js')
<script>
    var dropDownValues = [];

    $(document).on('click', '.delete', function (){
        axios.get('/forms/'+$(this).data("id")+'/delete')
            .then(function (response) {
                $("#"+response.data.form_id).remove();
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error.response);
            })
    })

    $(document).on('change', '#input-type', function (){
        if ($(this).val() === 'select'){
            $("#dropdown-items").show();
        }
    });

    $(document).on('click', "#add-possible-answer", function (){
        dropDownValues.push($("#drop-down-value").val());
        let htmlBuilder = '';
        dropDownValues.forEach(function (val, index){
            htmlBuilder += '<span class="badge badge-secondary mr-2">'+val+'</span>';
        });
        $("#drop-vals").html(htmlBuilder);
        $("#drop-down-value").val("");
    });
    $(document).on('click', "#modal", function (){
        $("#input-type").val(0);
        $("#label").val("");
        $("#update-input").replaceWith('<button type="button" class="btn btn-primary" id="add-input">Add Input</button>')
    })
    $(document).on('click', "#add-input", function (){
        storeOrUpdate('/forms/{{request()->segments()[1]}}', 'add')
    });
    $(document ).on('click', '#update-input', function (){
        let formId = $(this).data("id");
        storeOrUpdate('/forms/'+formId+'/update', 'update')
    });

    $(function (){
        $(".edit").on('click', function (){
            let formId = $(this).data("id");
            axios.get('/forms/{{request()->segment(2)}}/'+formId+'/edit')
                .then(function (response) {
                    $("#input-type").val(response.data.input.input_type);
                    $("#label").val(response.data.input.label);
                    $("#expected_answer").val(response.data.input.expected_answer);
                    $("#add-input").replaceWith('<button type="button" class="btn btn-primary" id="update-input" data-id="'+response.data.input.id+'">Update Input</button>')
                    $("#exampleModal").modal('show');
                    console.log(response);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error.response);
                })
        })
    })

    function storeOrUpdate(url, type){
        alert('Question stored.');
        let inputType = $("#input-type").val();
        let label = $("#label").val();
        if ($("#drop-down-value").val() != ''){
            dropDownValues.push($("#drop-down-value").val());
        }
        let expected_answer = $('#expected_answer').val();
        axios.post(url, {
            inputType: inputType,
            label: label,
            dropDownValues: dropDownValues,
            expected_answer: expected_answer
        })
            .then(function (response) {
                let dropDownItemsBuilder = "";

                if (response.data.input.drop_down_items.length > 0){
                    response.data.input.drop_down_items.forEach(function (val, ind){
                        dropDownItemsBuilder += '<span class="badge badge-primary mr-1">'+val.drop_down_value+'</span>';
                    })
                }
                let tableRow = '<tr id='+response.data.input.id+'>' +
                    '<td>'+response.data.input.input_type+'</td>'+
                    '<td>'+response.data.input.label+'</td>'+
                    '<td>'+dropDownItemsBuilder+'</td>'+
                    '<td>'+response.data.input.expected_answer+'</td>'+
                    '<td>'+
                    '<button class="btn btn-sm btn-warning edit" data-id='+response.data.input.id +'>Edit</button>' +
                    ' <button class="btn btn-sm btn-danger delete" data-id="'+response.data.input.id +'">Delete</button>' +
                    '</td>'+
                    '</tr>';

                if (type === "update"){
                    $("#"+response.data.input.id).replaceWith(tableRow)
                    // alert('Question updated.')
                }else{
                    $('tbody').append(tableRow)
                    // alert('Question stored.')
                }
                $("#drop-vals").html('');
                $("#exampleModal").modal('hide');
                dropDownValues = [];
                $("#dropdown-items").hide();
                console.log(response);
                
            })
            .catch(function (error) {
                console.log(error);
            });
    }

</script>
@endsection
