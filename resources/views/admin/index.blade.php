@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        @include('layouts.admin-menu')
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="text-align: center;">
            <h3><strong>Welcome to your admin panel.</strong></h3>
            <span>From here you can customize your portal and see your analytics.</span>
        </div>
    </div>
</div>
@endsection