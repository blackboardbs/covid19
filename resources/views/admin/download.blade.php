@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 2%; margin-left: 5%;">
    <button style="margin-bottom: 3%;" class="btn btn-primary" id="print" name="print" onclick="printPage()">Print</button>
    <br>
    <h1><strong>Welcome to {{$organization->name}}</strong></h1>
    <br>
    <h3><strong>Please scan the below QR code for COVID 19 screening and answer the questions.</strong></h3>
    <br>
    <div class="row" style="text-align: ceter;">
        {!! QrCode::size(1000)->generate(route('enter', $organization->id)) !!}
    </div>
    <h5 style="display: block; margin-top: -2%;"><strong>Powered by www.blackboardbs.com</strong></h5>
    
</div>
@endsection

<script>
    function printPage() {
        document.getElementById('print').style.display = "none";
        window.print();
        document.getElementById('print').style.display = "block";
    }
</script>