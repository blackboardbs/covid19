@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        @include('layouts.admin-menu')
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="border-style: groove; padding:2%; margin:2%;">
            <h3><strong>QR Code</strong></h3>
            <span>Scanning this code will start the screening process.</span>
            <div class="analytics" style="margin-top: 2%;">
                {{-- <img src="{!! QrCode::size(200)->generate(route('enter', $organization)) !!}" alt="QR Code"> --}}
                {!! QrCode::size(200)->generate(route('enter', $organization)) !!}

                <a href="{{ route('admin.download', $organization) }}" class="btn btn-primary" id="print" name="print">Print QR Code</a>
                {{-- <a href="{!! QrCode::size(200)->generate(route('enter', $organization)) !!}" download>Download QR code</a> --}}
            </div>
        </div>
    </div>
</div>
@endsection