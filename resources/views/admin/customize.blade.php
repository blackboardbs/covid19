@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        @include('layouts.admin-menu')
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="border-style: groove; padding:2%; margin:2%;">
            <h3><strong>Customize</strong></h3>
            <span>Customize your portal here.</span>
            <div class="custom" style="margin-top: 2%;">
                <form action="{{route('admin.customize_store')}}" method="post">
                    @csrf
                    {{-- <div class="form-group">
                        <label for="logo">Logo:</label>
                        <input type="file" class="form-control" id="logo" name="logo">
                    </div> --}}
                    <div class="form-group">
                        <label for="text">Text Colour:</label>
                        <input data-jscolor="" type="text" class="form-control" id="text" name="text" value="{{$config->text}}"><button class="btn btn-secondary" id="default_text" name="default_text" type="button" onclick="textDefault()">Default</button>
                    </div>
                    <div class="form-group">
                        <label for="background">Background Colour:</label>
                        <input data-jscolor="" type="text" class="form-control" id="background" name="background" value="{{$config->background}}"><button class="btn btn-secondary" id="default_background" name="default_background" type="button" onclick="backgroundDefault()">Default</button>
                    </div>
                    {{-- <div class="form-group">
                        <label for="menu">Menu Image:</label>
                        <input type="file" class="form-control" id="menu" name="menu">
                    </div> --}}
                    <button class="btn btn-primary" type="submit">Save</button>
                </form>
                
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    </div>
</div>
@endsection

@section('extra-js')

    <script>
            function textDefault(){
                $('#text').val('rgb(0, 0, 0)');
            }
    </script>

    <script>
        function backgroundDefault(){
            $('#background').val('rgb(255,255,255)');
        }
    </script>

@endsection