@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        @include('layouts.admin-menu')
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="border-style: groove; padding:2%; margin:2%;">
            <h3><strong>Analytics</strong></h3>
            <span>See usage and other statistics here.</span>
            <div class="analytics" style="margin-top: 2%;">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block;">
                    <div class="card" style="height: 200px;">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                          <strong>New Visitors:</strong>
                          <span>Past 14 days.</span>
                          <div class="count" style="text-align: center; margin-top: 20%;">
                            <h4><strong>{{$new_visitors}}</strong></h4>
                          </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                {{-- <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block;">
                    <div class="card" style="height: 200px;">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                          <strong>Visitors Allowed:</strong>
                          <span>Past 14 days.</span>
                          <div class="count" style="text-align: center; margin-top: 3%;">
                            <h4><strong>todo</strong></h4>
                          </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block;">
                    <div class="card" style="height: 200px;">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                          <strong>Visitors Denied:</strong>
                          <span>Past 14 days.</span>
                          <div class="count" style="text-align: center; margin-top: 20%;">
                            <h4><strong>todo</strong></h4>
                          </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div> --}}
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block;">
                    <div class="card" style="height: 200px;">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                          <strong>Returning Visitors:</strong>
                          <div class="count" style="text-align: center; margin-top: 20%;">
                            <h4><strong>{{$returning_visitors}}</strong></h4>
                          </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="display: inline-block;">
                    <div class="card" style="height: 200px;">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <strong>Total Visitors:</strong>
                            <div class="count" style="text-align: center; margin-top: 35%;">
                                <h4><strong>{{$visitors->count()}}</strong></h4>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="margin-top: 4%;">
                    <div class="card">
                        <div class="card-header">
                            <input hidden type="text" id="monday" name="monday" value="{{$monday}}">
                            <input hidden type="text" id="tuesday" name="tuesday" value="{{$tuesday}}">
                            <input hidden type="text" id="wednesday" name="wednesday" value="{{$wednesday}}">
                            <input hidden type="text" id="thursday" name="thursday" value="{{$thursday}}">
                            <input hidden type="text" id="friday" name="friday" value="{{$friday}}">
                            <input hidden type="text" id="saturday" name="saturday" value="{{$saturday}}">
                            <input hidden type="text" id="sunday" name="sunday" value="{{$sunday}}">
                        </div>
                        <div class="card-body">
                            <strong>Visitors this week:</strong>
                            <canvas id="myChart" width="400" height="400"></canvas>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-js')

<script>
var monday = $('#monday').val();
var tuesday = $('#tuesday').val();
var wednesday = $('#wednesday').val();
var thursday = $('#thursday').val();
var friday = $('#friday').val();
var saturday = $('#saturday').val();
var sunday = $('#sunday').val();

$( document ).ready(function() {
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            datasets: [{
                label: '# of Visitors this week',
                data: [monday, tuesday, wednesday, thursday, friday, saturday, sunday],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
});
</script>

@endsection