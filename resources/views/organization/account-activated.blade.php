@extends('layouts.app')

@section('content')

    <div class="container" style="align-content: center;">
        <div class="text-center" >
            <div class="alert alert-success">
                Thank you for creating your account. Your visitors can visit the provided link or scan the QR code below.
                <a href="{{route('enter', $organization)}}" >{{route('enter', $organization)}}</a>
                
            </div>

            <p></p>
            <a style="display: block; width: 25%;" href="{{route('forms.index', $organization)}}" class="btn btn-success">Customize Your Questions</a>
            <br>
            <a style="display: block; width: 25%;" href="{{ route('admin.download', $organization) }}" class="btn btn-primary" id="print" name="print">Print QR Code</a>
            {{-- <th>QR Code</th> --}}
            <td>{!! QrCode::size(200)->generate(route('enter', $organization)); !!}</td>
            
            
        </div>
    </div>

@endsection
