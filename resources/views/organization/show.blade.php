@extends('layouts.app')

@section('extra-css')



@endsection

@section('content')
<div class="container-fluid" style="margin-top: 2%; display: inline-block;">
    <div class="row">
    @include('layouts.admin-menu')
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="border-style: groove; padding:2%; margin:2%;">
        <h3><strong>Organization Profile</strong></h3>
        <span>View and edit your organization here.</span>
        <div class="custom" style="margin-top: 2%;">
            
            
        
    {{-- <div class="alert alert-info">
        You will recieve an email where you can activate your organization
    </div> --}}
    @auth
        @if (Auth()->user()->id == $organization->user_id)
            
        @endif
    @else 
        <div class="alert alert-info">
            You will recieve an email where you can activate your organization
        </div>
    @endauth
    {{-- <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="float:right; padding:2%; margin:2%;"> --}}
        <div class="text-center" style="margin-bottom: 2%;">
            <a href="{{route('enter', $organization)}}" class="btn btn-info mr-3">Visitor Screen</a>
            <a href="{{route('forms.index', $organization)}}" class="btn btn-primary">Customize Your Questions</a>
            <a href="{{route('organization.edit', $organization)}}" class="btn btn-success ml-3">Edit</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th colspan="4" class="text-center">Organization</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Organization Name</th>
                <td>{{$organization->name}}</td>
                <th>Registration Number/ID Number</th>
                <td>{{$organization->registration_number}}</td>
            </tr>
            <tr>
                <th scope="row">Vat Number</th>
                <td>{{$organization->vat_number}}</td>
                <td>Contact Person</td>
                <td>{{$organization->user->name??''}}</td>
            </tr>
            <tr>
                <th scope="row">Billing Address</th>
                <td colspan="3"><ul class="list-group">
                        <li class="list-group-item">{{$organization->address->address_line1??''}}</li>
                        @isset($organization->address->address_line2)
                            <li class="list-group-item">{{$organization->address->address_line2??''}}</li>
                        @endisset
                        @isset($organization->address->suburb)
                            <li class="list-group-item">{{$organization->address->suburb??''}}</li>
                        @endisset
                        @isset($organization->address->city)
                            <li class="list-group-item">{{$organization->address->city??''}}</li>
                        @endisset
                        @isset($organization->address->province)
                            <li class="list-group-item">{{$organization->address->province??''}}</li>
                        @endisset
                        @isset($organization->address->country)
                            <li class="list-group-item">{{$organization->address->country??''}}</li>
                        @endisset
                        @isset($organization->address->zip_code)
                            <li class="list-group-item">{{$organization->address->zip_code??''}}</li>
                        @endisset
                    </ul></td>
            </tr>
            <tr>
                <td>Exit Link</td>
                <td>{{$organization->exit_link??''}}</td>
                <th scope="row">Exit Link Label</th>
                <td>{{$organization->exit_link_label}}</td>

            </tr>
            <tr>
                <th scope="row">Welcome Message</th>
                <td colspan = 3>{{$organization->welcome_message}}</td>
            </tr>
            <tr>
                <th>Organization Logo</th>
                <td><img src="{{asset('storage/organization/'.$organization->logo)}}" alt="{{$organization->name}}" class="img-thumbnail"></td>
                <th>QR Code</th>
                <td>{!! QrCode::size(200)->generate(route('enter', $organization)); !!}
                    <a href="{{ route('admin.download', $organization) }}" class="btn btn-primary" id="print" name="print">Print QR Code</a></td>
            </tr>
            </tbody>
        </table>
        @auth
        @if (Auth()->user()->id == $organization->user_id && Auth()->user()->purchase_status == 'purchased')
            {{-- To cancel your subscription <a href="{{ route('cancel') }}">click here</a>. --}}
            To cancel your subscription <a href="{{ route('organization.cancel') }}">click here</a>.
            <br>
            <small>Note: This will revoke your access to the Covid App immediately</small>
            {{-- <form action="https://api.payfast.co.za/subscriptions/dc0521d3-55fe-269b-fa00-b647310d760f/cancel" method="post">
                <input type="hidden" name="merchant-id" value="10013304">
                <input type="hidden" name="version" value="v1">
                <input type="hidden" name="timestamp" value="{{ $date }}">
                <input type="hidden" name="signature" value="{{ $signature }}">
                <button class="btn btn-danger">Cancel</button>
            </form> --}}
        @endif
        @endauth
    </div>
</div>
</div>
    {{-- </div> --}}
</div>

@endsection
