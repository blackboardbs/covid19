@extends('layouts.app')

@section('extra-css')
    


@endsection

@section('content')
    <div class="container">
        <h3>View Organizations <a href="{{route('organization.create')}}" class="float-right btn btn-sm btn-primary">Add Organization</a></h3>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Registration Number</th>
                <th scope="col">Contact Person</th>
                <th scope="col">Contact Number</th>
                <th scope="col">Contact Email</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @forelse($organizations as $organization)
                <tr>
                    <td scope="row"><a href="{{route('organization.show', $organization->id)}}">{{$organization->name}}</a></td>
                    <td>{{$organization->registration_number}}</td>
                    <td>{{$organization->user->name??''}}</td>
                    <td>{{$organization->user->contact_number??''}}</td>
                    <td>{{$organization->user->email??''}}</td>
                    <td>
                        <a href="{{route('organization.edit', $organization->id)}}" class="btn btn-sm btn-success">Edit</a>
                        <form action="{{route('organization.destroy', $organization->id)}}" method="post">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6">No organizations yet</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

@endsection
