@extends('layouts.app')

@section('extra-css')

<style>
    body{
            margin:0;
            color:#6a6f8c;
            background:#c8c8c8;
            font:600 16px/18px 'Open Sans',sans-serif;
        }

</style>

@endsection

@section('content')
{{-- @if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
    @endif --}}

    <div class="container">
        <h3 style="text-align: center">Capture An Organization</h3>
        <small>Compulsory fields are indicated by <h5 style="color: red; display: inline-block;">*</h5></small>
        <form action="{{route('organization.store')}}" method="POST"  enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <br>
                <h5 style="color: red; display: inline-block;">*</h5><label for="name" class="form-label">Organization Name</label>
                <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                @error('name')
                    <div class="alert alert-danger">Orginization name required.</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="registration" class="form-label"> Company Registration Number/ID Number</label>
                <input type="text" class="form-control" name="registration" id="registration" value="{{ old('registration') }}">
            </div>
            <div class="form-group">
                <label for="vat_number" class="form-label">Vat Number</label>
                <input type="text" class="form-control" name="vat_number" id="vat_number" value="{{ old('vat_number') }}">
            </div>
             <div class="form-group">
                <h5 style="color: red; display: inline-block;">*</h5><label for="user" class="form-label">Admin User</label>
                <div id="user_field">
                    <input type="button" class="btn btn-sm btn-primary" id="user" value="Capture Admin User" data-toggle="modal" data-target="#userModal" style="width: 15%;">
                    @error('user_id')
                        <div class="alert alert-danger">Please capture an admin user first.</div>
                    @enderror
                </div>
            </div>
            {{--<input type="hidden" name="user_id" value="{{$user_id}}" >--}}
            <div class="form-group">
                <label for="address" class="form-label">Address</label>
                <div id="address_field">
                    <input type="button" class="btn btn-sm btn-primary" id="address" value="Capture Address" data-toggle="modal" data-target="#addressModal" style="width: 15%;">
                    {{-- @error('user_id')
                        <div class="alert alert-danger">Please capture an address first.</div>
                    @enderror --}}
                </div>
            </div>
            <div class="form-group">
                <label for="welcome_message" class="form-label">Welcome message to users</label>
                <textarea name="welcome_message" class="form-control" id="welcome_message" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="logo" class="form-label">Logo</label>
                <input type="file" name="logo" class="form-control-file" id="logo">
            </div>
            <div class="form-group">
                <label for="exit_link" class="form-label">Exit Link (Your url)</label>
                <input type="text" name="exit_link" class="form-control" id="exit_link">
            </div>
            <div class="form-group">
                <label for="exit_link_text" class="form-label">Exit Link Label</label>
                <input type="text" name="exit_link_text" class="form-control" id="exit_link_text">
            </div>
            <div class="form-group form-check text-center">
                <input type="checkbox" name="accept_terms" class="form-check-input" id="accept_terms" value="1">
                <h5 style="color: red; display: inline-block;">*</h5><label class="form-check-label" for="accept_terms"><a href="{{ route("term") }}" class="w3-btn w3-black" target="_blank">Accept Terms and Conditions</a></label>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary" id="capture-organisation">Capture Organization</button>
            </div>
        </form>

        <!-- User Modal -->
        <div class="modal fade" id="userModal" tabindex="-1" aria-labelledby="userModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="userModalLabel">Capture Admin User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="user_name">Contact Person Name</label>
                            <input type="text" class="form-control" id="user_name">
                        </div>
                        <div class="form-group">
                            <label for="email">Contact Person Email address</label>
                            <input type="text"  class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="text" class="form-control" id="contact_number">
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Password</label>
                            <input type="password" class="form-control" id="password">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="capture-user">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Address Modal -->
        <div class="modal fade" id="addressModal" tabindex="-1" aria-labelledby="addressModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addressModalLabel">Capture Address</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="address_line1">Address Line 2</label>
                            <input type="text" class="form-control" id="address_line1">
                        </div>
                        <div class="form-group">
                            <label for="address_line2">Address Line 1</label>
                            <input type="text" class="form-control" id="address_line2">
                        </div>
                        <div class="form-group">
                            <label for="suburb">Suburb</label>
                            <input type="text" class="form-control" id="suburb">
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city">
                        </div>
                        <div class="form-group">
                            <label for="province">Province/State</label>
                            <input type="text" class="form-control" id="province">
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" class="form-control" id="country">
                        </div>
                        <div class="form-group">
                            <label for="zip_code">Zip Code</label>
                            <input type="text" class="form-control" id="zip_code">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="capture-address">Capture Address</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input id="users" name="users" hidden value="{{ $users }}">

@endsection

@section('extra-js')

    <script>
        $(function (){
            $("#capture-user").on('click', function (){
                let userName = $("#user_name");
                let userEmail = $("#email");
                let password = $("#password");
                let contactNumber = $("#contact_number");

                let users = $("#users").val();

                if (userName.val() == ''){
                    userName.css('border', '1px solid red')
                        .after('<div class="mt-1 text-danger">Name is required</div>');
                    return false;
                }
                $.each(JSON.parse(users), function(user, email) {
                    // console.log(email);
                    if (userEmail.val() == email){
                        userEmail.css('border', '1px solid red')
                        .after('<div class="mt-1 text-danger">User already exists</div>');
                    return false;
                    }
                });
                if (!IsEmail(userEmail.val())){
                    userEmail.css('border', '1px solid red')
                        .after('<div class="mt-1 text-danger">Please enter a valid email</div>');
                    return false;
                }

                if (password.val() == ''){
                    password.css('border', '1px solid red')
                        .after('<div class="mt-1 text-danger">Password is required</div>');
                    return false;
                }
                if (password.val().length < 5){
                    password.css('border', '1px solid red')
                        .after('<div class="mt-1 text-danger">Password must be 5 or more characters</div>');
                }

                if (contactNumber.val() == ''){
                    contactNumber.css('border', '1px solid red')
                        .after('<div class="mt-1 text-danger">Contact Number is required</div>');
                }

                axios.post('/user', {
                    userName: userName.val(),
                    userEmail: userEmail.val(),
                    password: password.val(),
                    contactNumber: contactNumber.val(),
                    organization: 1
                })
                    .then(function (response) {
                        $("#user").replaceWith(
                            '<input type="hidden" name="user_id" value="'+response.data.user.id+'" >'+
                            '<span style="color:blue;">'+response.data.user.name+'</span>'+
                            '<div class="alert alert-success">Admin User Captured</div>'
                        )
                        $("#userModal").modal('hide')
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            })
        })

        $(function (){
            $("#capture-address").on('click', function (){
                let addressLine1 = $("#address_line1");
                if (addressLine1.val().length < 1){
                    addressLine1.css('border', '1px solid red')
                        .after('<div class="text-danger">Address Line 1 is required</div>');
                }

                axios.post('{{route('address.store')}}', {
                    addressLine1: addressLine1.val(),
                    addressLine2: $("#address_line2").val(),
                    suburb: $("#suburb").val(),
                    city: $("#city").val(),
                    province: $("#province").val(),
                    country: $("#country").val(),
                    zipCode: $("#zip_code").val(),
                })
                    .then(function (response) {
                        let listBuilder = '<li class="list-group-item">'+response.data.address.address_line1+'</li>';
                        if (response.data.address.address_line2 != null) listBuilder += '<li class="list-group-item">'+response.data.address.address_line2+'</li>'
                        if (response.data.address.suburb != null) listBuilder += '<li class="list-group-item">'+response.data.address.suburb+'</li>'
                        if (response.data.address.city != null) listBuilder += '<li class="list-group-item">'+response.data.address.city+'</li>'
                        if (response.data.address.province != null) listBuilder += '<li class="list-group-item">'+response.data.address.province+'</li>'
                        if (response.data.address.country != null) listBuilder += '<li class="list-group-item">'+response.data.address.country+'</li>'
                        if (response.data.address.zip_code != null) listBuilder += '<li class="list-group-item">'+response.data.address.zip_code+'</li>'
                        $("#address").replaceWith(
                            '<ul class="list-group">' +
                            listBuilder +
                            '</ul>' +
                            '<input type="hidden" name="address_id" value="'+ response.data.address.id+'">'+
                            '<div class="alert alert-success">Address Captured</div>'
                        )

                        $("#addressModal").modal('hide');
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            })
        })

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                return false;
            } else {
                return true;
            }
        }
    </script>

@endsection
