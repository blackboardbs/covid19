<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <h3>Capture An Organization</h3>
    <form action="{{route('organization.update', $organization->id)}}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control" id="name" value="{{$organization->name}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="registration" class="col-sm-2 col-form-label">Registration Number/ID Number</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="registration" id="registration"  value="{{$organization->registration_number}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="vat_number" class="col-sm-2 col-form-label">Vat Number</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="vat_number" id="vat_number" value="{{$organization->vat_number}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="user" class="col-sm-2 col-form-label">Capture User</label>
            <div class="col-sm-10" id="user_field">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$organization->user->name??''}}">
                <input type="hidden" name="user_id" id="user" value="{{$organization->user_id}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="address" class="col-sm-2 col-form-label">Address</label>
            <div class="col-sm-10" id="address_field">
                <input type="button" class="btn btn-sm btn-primary mb-2" id="address" value="Edit Address" data-toggle="modal" data-target="#addressModal">
                <input type="hidden" name="address_id" value="{{$organization->address_id}}">
                <ul class="list-group">
                    <li class="list-group-item">{{$organization->address->address_line1??''}}</li>
                    @isset($organization->address->address_line2)
                        <li class="list-group-item">{{$organization->address->address_line2??''}}</li>
                    @endisset
                    @isset($organization->address->suburb)
                        <li class="list-group-item">{{$organization->address->suburb??''}}</li>
                    @endisset
                    @isset($organization->address->city)
                        <li class="list-group-item">{{$organization->address->city??''}}</li>
                    @endisset
                    @isset($organization->address->province)
                        <li class="list-group-item">{{$organization->address->province??''}}</li>
                    @endisset
                    @isset($organization->address->country)
                        <li class="list-group-item">{{$organization->address->country??''}}</li>
                    @endisset
                    @isset($organization->address->zip_code)
                        <li class="list-group-item">{{$organization->address->zip_code??''}}</li>
                    @endisset
                </ul>
            </div>
        </div>
        <div class="form-group row">
            <label for="welcome_message" class="col-sm-2 col-form-label">Welcome Message</label>
            <div class="col-sm-10">
                <textarea name="welcome_message" class="form-control" id="welcome_message" rows="3">{{$organization->welcome_message}}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="logo" class="col-sm-2 col-form-label">Logo</label>
            <div class="col-sm-10">
                <img src="{{asset('storage/organization/'.$organization->logo)}}" class="img-thumbnail">
                <input type="file" name="logo" class="form-control-file" id="logo">
            </div>
        </div>
        <div class="form-group row">
            <label for="exit_link" class="col-sm-2 col-form-label">Exit Link (Your url)</label>
            <div class="col-sm-10">
                <input type="text" name="exit_link" class="form-control" id="exit_link" value="{{$organization->exit_link}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="exit_link_text" class="col-sm-2 col-form-label">Exit Link Label</label>
            <div class="col-sm-10">
                <input type="text" name="exit_link_text" class="form-control" id="exit_link_text" value="{{$organization->exit_link_label}}">
            </div>
        </div>
        {{--<div class="form-group form-check text-center">
            <input type="checkbox" name="accept_terms" class="form-check-input" id="accept_terms" value="1">
            <label class="form-check-label" for="accept_terms">Accept Terms and Conditions</label>
        </div>--}}
        <div class="text-center">
            <button type="submit" class="btn btn-primary" id="capture-organisation">Update Organization</button>
        </div>
    </form>
    <!-- Address Modal -->
    <div class="modal fade" id="addressModal" tabindex="-1" aria-labelledby="addressModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addressModalLabel">Edit Address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="address_line1">Address Line 2</label>
                        <input type="text" class="form-control" id="address_line1" value="{{$organization->address->address_line1??''}}">
                    </div>
                    <div class="form-group">
                        <label for="address_line2">Address Line 1</label>
                        <input type="text" class="form-control" id="address_line2" value="{{$organization->address->address_line2??''}}">
                    </div>
                    <div class="form-group">
                        <label for="suburb">Suburb</label>
                        <input type="text" class="form-control" id="suburb"  value="{{$organization->address->suburb??''}}">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" value="{{$organization->address->suburb??''}}">
                    </div>
                    <div class="form-group">
                        <label for="province">Province/State</label>
                        <input type="text" class="form-control" id="province" value="{{$organization->address->province??''}}">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" class="form-control" id="country" value="{{$organization->address->country??''}}">
                    </div>
                    <div class="form-group">
                        <label for="zip_code">Zip Code</label>
                        <input type="text" class="form-control" id="zip_code" value="{{$organization->address->zip_code??''}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="capture-address">Update Address</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    $(function (){
        $("#capture-address").on('click', function (){
            let addressLine1 = $("#address_line1");
            if (addressLine1.val().length < 1){
                addressLine1.css('border', '1px solid red')
                    .after('<div class="text-danger">Address Line 1 is required</div>');
            }

            axios.patch('{{route('address.update', $organization->address_id)}}', {
                addressLine1: addressLine1.val(),
                addressLine2: $("#address_line2").val(),
                suburb: $("#suburb").val(),
                city: $("#city").val(),
                province: $("#province").val(),
                country: $("#country").val(),
                zipCode: $("#zip_code").val(),
            })
                .then(function (response) {
                    let listBuilder = '<li class="list-group-item">'+response.data.address.address_line1+'</li>';
                    if (response.data.address.address_line2 != null) listBuilder += '<li class="list-group-item">'+response.data.address.address_line2+'</li>'
                    if (response.data.address.suburb != null) listBuilder += '<li class="list-group-item">'+response.data.address.suburb+'</li>'
                    if (response.data.address.city != null) listBuilder += '<li class="list-group-item">'+response.data.address.city+'</li>'
                    if (response.data.address.province != null) listBuilder += '<li class="list-group-item">'+response.data.address.province+'</li>'
                    if (response.data.address.country != null) listBuilder += '<li class="list-group-item">'+response.data.address.country+'</li>'
                    if (response.data.address.zip_code != null) listBuilder += '<li class="list-group-item">'+response.data.address.zip_code+'</li>'
                    $(".list-group").replaceWith(
                        '<ul class="list-group">' +
                        listBuilder +
                        '</ul>' +
                        '<input type="hidden" name="address_id" value="'+ response.data.address.id+'">'
                    )

                    $("#addressModal").modal('hide');
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                })
        })
    })
</script>
</body>
</html>
