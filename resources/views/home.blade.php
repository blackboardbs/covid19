@extends('layouts.app')

@section('content')

<div class="login-wrap">
    	<div class="login-html text-center">
            <img class="img" src="{{asset('storage/organization/'.$organization->logo)}}" alt=""> 
            @if($organization->account_active == 1)
                <label class="label"><h3>Welcome to {{$organization->name}}</h3></label>
                <p class="text-black">{{$organization->welcome_message}}</p>

                <form action="{{route('capture.visit', $organization)}}" method="post">
                    @csrf
                    <label class="label">Please enter your email to continue</label> <br><br>

                <input class="input" type="email" name="email" style="color: black;" required><br><br>
                <button type="submit" class="button">Continue</button>
                </form>
            @else
                <p class="text-white">{{$organization->name}} have not activated their account yet. Please talk to the manager to check their mail for the activation mail</p>
            @endif

    	</div>
    </div>

@endsection


@section('extra-css')

<style>
    .button{
    	border:none;
    	padding:15px 20px;
    	border-radius:25px;
    	background:blue;
        width:100%;
    	color:#fff;
        text-transform:uppercase;

    }

    .button:hover{
        background-color: grey;
        }

    .input{
    	border:none;
    	padding:15px 20px;
    	border-radius:25px;
    	background:whitesmoke;
        width:100%;
    	color:#fff;


    }

    .label{
    	color:black;
    	font-size:20px;
    }

    .img{

    	margin:auto;
        max-width:150px;
        height: auto;
    	min-height:50px;
    	position:relative;
        display: block;
        padding-bottom: 15px;
        overflow: hidden;
        object-fit: cover;
    }
    body{
    	margin:0;
    	color:#6a6f8c;
    	background:#c8c8c8;
    	font:600 16px/18px 'Open Sans',sans-serif;
    }
    *,:after,:before{box-sizing:border-box}
    .clearfix:after,.clearfix:before{content:'';display:table}
    .clearfix:after{clear:both;display:block}
    a{color:inherit;text-decoration:none}

    .login-wrap{
    	width:100%;
    	margin:auto;
    	max-width:525px;
    	min-height:670px;
    	position:relative;
    	/* background:url(https://raw.githubusercontent.com/khadkamhn/day-01-login-form/master/img/bg.jpg) no-repeat center; */
    	box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
    }
    .login-html{
    	width:100%;
    	height:100%;
    	position:absolute;
    	padding:90px 70px 50px 70px;
    	/* background:rgba(40,57,101,.9); */
    }

    </style>

@endsection



