@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="text-center">
            <div class="alert alert-danger">
                Your transaction has been cancelled.
                <br>
                To continue using the app please purchase a license.
            </div>

            {{-- <p><a href="{{ route('login') }}">Login</a></p> --}}
        </div>
    </div>

@endsection