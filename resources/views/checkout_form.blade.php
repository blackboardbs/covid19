@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="text-center">
            <div style="margin-top: 4%;">
                <p>You are about to purchase a license to continue using the app.</p>
                <p>By clicking Continue you will be redirected to the payment page where you will finalize the transaction.</p>
                {!!$payform!!}
            {{-- <a href="{{route('cancel')}}">cancel</a> --}}
            </div>
        </div>
    </div>

@endsection