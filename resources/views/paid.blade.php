@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="text-center">
            <div class="alert alert-success">
                Thank you for puchasing the license your access has been re-enabled. 
                <br>
                You can continue to login with the link below
            </div>

        <p><a href="{{ route('login') }}">Login</a></p>
        </div>
    </div>

@endsection