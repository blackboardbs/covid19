@extends('layouts.app')

@section('content')
    <div class="container">
        <h4 class="text-center">Capture Visit</h4>
        <form action="{{route('visit.store', $organization)}}" method="POST">
            @csrf
            <input hidden type="text" name="organization_id" class="form-control" id="organization_id" value="{{$organization->id}}">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="visitor_name" class="form-control" id="name" value="{{$user->name??''}}">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email1" class="form-control" id="email1" disabled value="{{$user->email??$email}}">
                <input type="email" name="email" class="form-control" id="email" hidden value="{{$user->email??$email}}">
            </div>
            <div class="form-group">
                <label for="cellphone_number">Cellphone</label>
                <input type="text" name="cellphone_number" class="form-control" id="cellphone_number" value="{{$user->contact_number??''}}">
            </div>
            <div class="form-group" id="guests-wrapper">
                <label for="">Are you accompanied by any guests/children?</label><br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input guest" type="radio" name="guests" id="guests_yes" value="1">
                    <label class="form-check-label" for="guests_yes">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input guest" type="radio" name="guests" id="guests_no" value="0">
                    <label class="form-check-label" for="guests_no">No</label>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Next Step</button>
            </div>
        </form>
    </div>
@endsection

@section('extra-js')
    <script>
        $(function (){
            $('input[type=radio][name=guests]').change(function() {
                if ($(this).val() == 1){

                    $("#guests-wrapper").after(
                        '<div class="form-group" id="guests-previous">' +
                        '    <label for="">Have you been accompanied by any of these guests before?</label><br>' +
                        '    <div class="form-check form-check-inline">' +
                        '       <input class="form-check-input" type="radio" name="guests_previous" id="guests_previous_yes" value="1">' +
                        '       <label class="form-check-label" for="guests_previous_yes">Yes</label>' +
                        '    </div>' +
                        '    <div class="form-check form-check-inline">' +
                        '        <input class="form-check-input" type="radio" name="guests_previous" id="guests_previous_no" value="0">' +
                        '        <label class="form-check-label" for="guests_previous_no">No</label>' +
                        '     </div>' +
                        '</div>'
                    );
                }

            });

            $(document).on('change', 'input[type=radio][name=guests_previous]',function() {
                if ($(this).val() == 0){
                    let optionsBuider = '<option>Select number of your guests</option>';
                    for (let i = 1; i <= 10; ++i){
                        optionsBuider += '<option value="'+i+'">'+i+'</option>';
                    }
                    $("#guests-previous").after(
                        '<div class="form-group">' +
                        '    <label for="guests_number">Number of accompanying guests</label>' +
                        '    <select class="form-control" name="guests_number" id="guests_number">' +
                        optionsBuider +
                        '    </select>' +
                        '  </div>'
                    );
                }
            })
        })
    </script>
@endsection
