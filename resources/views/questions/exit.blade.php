@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="text-center">
            <h3><strong>Welcome to {{ $organization->name }}</strong></h3>
            <br>
            <strong>{{$organization->welcome_message}}</strong>
            <div class="mt-3">
                @if (isset($organization->exit_link))
                    <a href="http://{{$organization->exit_link}}" class="btn btn-primary" target="_blank">{{$organization->exit_link_label}}</a>
                @endif
            </div>
            <div class="mt-2">
                <a href="{{route('enter', $organization)}}" class="btn btn-info">Done</a>
            </div>
        </div>
    </div>

@endsection
