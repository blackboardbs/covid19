@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="text-center">
            <strong>Access Denied, one or more answers do not meet the criteria.</strong>
            <div class="mt-2">
                <a href="{{route('enter', $organization)}}" class="btn btn-info">Next Visitor</a>
            </div>
        </div>
    </div>

@endsection