@extends('layouts.app')

@section('content')

    <div class="container">
        <h3>Questions</h3>
        <form action="{{route('answers.store', $organization)}}" method="POST">
            @csrf
            @php
                $guests_left = session('guests_number_left');
                $number_of_guests = session('guests_number');
            @endphp

            @if(session()->has('guest_number'))
                <h3>Guest Number {{session('guest_number')}}</h3>
                <div class="form-group">
                    <label for="guest_name">Guest Name</label>
                    <input type="text" name="guest_name" class="form-control" id="guest_name">
                </div>
            @endif

            @forelse($questions as $question)
                @switch($question['input_type'])
                    @case('text')
                        <div class="form-group">
                            <label for="{{$question['id']}}">{{$question['label']}}</label>
                            <input type="text" required name="{{strtolower(str_replace(' ','_', $question['label']))}}" class="form-control" id="{{$question['id']}}" value="{{$question['value']??''}}">
                        </div>
                    @break 

                    @case('select')
                        <div class="form-group">
                            @if ($question['id'] == 3)
                                <label for="{{$question['id']}}">{{$question['label']}}</label>
                                <select required multiple class="form-control" name="{{strtolower(str_replace(' ','_', $question['label']))}}" id="{{$question['id']}}">
                                    <option>Select your answer</option>
                                    @forelse($question["dropdown_items"] as $item)
                                        <option {{($question['value'] == $item['id'])?'selected':''}} value="{{$item['id']}}">{{$item["value"]}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            @else
                                <label for="{{$question['id']}}">{{$question['label']}}</label>
                                <select required class="form-control" name="{{strtolower(str_replace(' ','_', $question['label']))}}" id="{{$question['id']}}">
                                    <option>Select your answer</option>
                                    @forelse($question["dropdown_items"] as $item)
                                        <option {{($question['value'] == $item['id'])?'selected':''}} value="{{$item['id']}}">{{$item["value"]}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            @endif
                        </div>
                    @break

                    
                    @case('radio')
                        <div hidden>
                        Second case...
                        </div>
                    @break
                    

                    
                    @case('date')
                        <div hidden>
                        Second case...
                        </div>
                    @break
                    
                @endswitch
            @empty
            @endforelse

            <div class="text-center">
                <button id="save" type="submit" class="btn btn-primary">@if((isset($guests_left) && $guests_left > 0) || (session()->has('guests_number') && session('guests_number') > 0))Next Guest @else Save @endif</button>
            </div>
        </form>
        <a id="denied" href="{{ route('denied', $organization) }}" class="nav-link" hidden>denied</a>
    </div>

@endsection