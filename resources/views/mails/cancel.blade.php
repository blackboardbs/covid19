@component('mail::message')
# Cancel Request

The following user has requested to end their subscription for Covid 19 App.

{{$user->name}}
{{$user->email}}

Thanks,<br>
Blackboard
@endcomponent
