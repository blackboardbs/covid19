@component('mail::message')
# Welcome!

Your account to use Blackboard Covid-19 App has been set up using this email address.

**Click the button below to go to login:**

@component('mail::button', ['url' => route('login')])
Login
@endcomponent

{!! QrCode::size(200)->generate(route('login')); !!}

Or if you can't click on the button, copy/paste the link below into your web browser

{{route('login')}}

Thanks,<br>
{{ $organization->name }}
@endcomponent
