@component('mail::message')
# Trial Account Expiring

**Your trial account will expire within the next 4 days.** To continue using this service please renew your license for only R299.99 p/m.

**Click the button below to renew your account now:**

@component('mail::button', ['url' => route('pay_page', $user->id)])
Buy license
@endcomponent

Or if you can't click on the button, copy/paste the link below into your web browser.

{{ route('pay_page', $user->id) }}

Thanks,<br>
Blackboard
@endcomponent