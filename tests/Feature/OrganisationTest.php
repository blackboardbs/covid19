<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrganisationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function can_access_organisation()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/organization');

        $response->assertStatus(200);
    }
}
