<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();;
            $table->string('registration_number')->nullable();
            $table->string('vat_number')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('address_id')->nullable();
            $table->text('welcome_message')->nullable();
            $table->string('logo')->default('default.png')->nullable();
            $table->string('exit_link')->nullable();
            $table->string('exit_link_label')->nullable();
            $table->boolean('account_active')->nullable();
            $table->dateTime('terms_accepted')->nullable();
            $table->string('ip_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization');
    }
}
