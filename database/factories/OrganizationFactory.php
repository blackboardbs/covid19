<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Organization;
use Faker\Generator as Faker;

$factory->define(Organization::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'registration_number' => $faker->randomNumber(8),
        'user_id' => 1,
        'address_id' => $faker->randomDigit
    ];
});
