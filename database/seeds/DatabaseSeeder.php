<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(roles_table_seeder::class);
        $this->call(drop_down_items_table_seeder::class);
        $this->call(forms_table_seeder::class);
    }
}
