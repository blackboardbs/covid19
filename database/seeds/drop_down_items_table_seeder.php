<?php

use Illuminate\Database\Seeder;

class drop_down_items_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drop_down_items')->insert([
            'form_id' => 2,
            'drop_down_value' => 'Yes',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 2,
            'drop_down_value' => 'No',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Temperature over 38',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Fever in the last 14 days',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Cough',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Chills',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Body Pains',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Sore Throat',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Shortness of breath',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Nausea/Vomiting',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Diarrhoea',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'General Weakness',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'Irritability/Confusion',
        ]);

        DB::table('drop_down_items')->insert([
            'form_id' => 3,
            'drop_down_value' => 'None',
        ]);

        // DB::table('drop_down_items')->insert([
        //     'form_id' => 4,
        //     'drop_down_value' => 'Yes',
        // ]);

        // DB::table('drop_down_items')->insert([
        //     'form_id' => 4,
        //     'drop_down_value' => 'No',
        // ]);
    }
}
