<?php

use Illuminate\Database\Seeder;

class forms_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forms')->insert([
            'organization_id' => 0,
            'input_type' => 'text',
            'label' => 'Current Temperature',
            'expected_answer' => null,
        ]);

        DB::table('forms')->insert([
            'organization_id' => 0,
            'input_type' => 'select',
            'label' => 'Have you had contact with any possible Covid cases in the last 14 days?',
            'expected_answer' => 'No',
        ]);

        DB::table('forms')->insert([
            'organization_id' => 0,
            'input_type' => 'select',
            'label' => 'Possible Symptoms',
            'expected_answer' => null,
        ]);

        // DB::table('forms')->insert([
        //     'organization_id' => 0,
        //     'input_type' => 'select',
        //     'label' => 'I confirm that all information provided is valid.',
        //     'expected_answer' => 'Yes',
        // ]);
    }
}
