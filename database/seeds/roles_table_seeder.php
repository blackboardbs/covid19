<?php

use Illuminate\Database\Seeder;

class roles_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'description' => 'Admin',
        ]);

        DB::table('roles')->insert([
            'name' => 'User',
            'description' => 'User',
        ]);
    }
}
