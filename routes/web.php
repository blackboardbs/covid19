<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Route::get('/', 'OrganizationController@create');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/term', 'TermController@index')->name('term');

Route::get('/admin/download/qr/{organization}', 'AdminController@download_qr')->name('admin.download');

Route::get('/paid', 'PaymentController@paid')->name('paid');
Route::post('/response', 'PaymentController@itn')->name('itn');
Route::get('/cancel', 'PaymentController@cancel')->name('cancel');

Auth::routes();

Route::post('capture/visit/{organization}', 'VisitorController@captureVisit')->name('capture.visit');
Route::post('visit/{organization}', 'VisitorController@store')->name('visit.store');
Route::resource('organization', 'OrganizationController');
Route::get('organization/activate/{organization}', 'OrganizationController@welcome')->name('organization.activate');
Route::get('{organization}', 'QuestionController@home')->name('enter');
Route::get('/request/cancel', 'OrganizationController@cancel')->name('organization.cancel');
Route::get('questions/{organization}', 'QuestionController@questions')->name('questions');
Route::post('answer/store/{organization}', 'QuestionController@answers')->name('answers.store');
Route::get('exit/{organization}', 'QuestionController@exit')->name('exit');
Route::get('denied/{organization}', 'QuestionController@denied')->name('denied');
Route::get('forms/{organization}', 'FormController@index')->name('forms.index');
Route::Post('forms/{organization}', 'FormController@store')->name('forms.store');
Route::get('forms/{organization}/{form_id}/edit', 'FormController@edit')->name('forms.edit');
Route::post('forms/{form}/update', 'FormController@update')->name('forms.update');
Route::get('forms/{form}/delete', 'FormController@destroy')->name('forms.delete');
Route::get('user/logout', 'UserController@logout')->name('user.logout');
Route::resource('user', 'UserController');
Route::resource('address', 'AddressController');

Route::get('admin/index', 'AdminController@index')->name('admin.index')->middleware('auth');
Route::get('admin/customize', 'AdminController@customize')->name('admin.customize')->middleware('auth');
Route::post('admin/customize/store', 'AdminController@customize_store')->name('admin.customize_store')->middleware('auth');
Route::get('admin/analytics', 'AdminController@analytics')->name('admin.analytics')->middleware('auth');
Route::get('admin/barcode', 'AdminController@barcode')->name('admin.barcode')->middleware('auth');
Route::post('/admin/config', 'ConfigController@update')->name('admin.config');


Route::post('/member', 'UserController@member')->name('member.create');

Route::get('/process/payment/{user_id}', 'PaymentController@confirmPayment')->name('pay_page');



