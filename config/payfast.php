<?php

return

/*
|--------------------------------------------------------------------------
| Merchant Settings
|--------------------------------------------------------------------------
| All Merchant settings below are for example purposes only. for more info
| see www.payfast.co.za. The Merchant ID and Merchant Key can be obtained
| from your payfast.co.za account.
|
*/
[
    'testing' => env('PG_PAYFAST_TESTING', true), // Set to false when in production.
    'currency' => 'ZAR',
    'merchant' => [
        'merchant_id' => env('PG_PAYFAST_MERCHANT_ID', '10013304'),
        'merchant_key' => env('PG_PAYFAST_MERCHANT_KEY', '53g6w0bg6eviy'),
        'return_url' => env('PG_PAYFAST_RETURN_URL', 'http://e4d6c515.ngrok.io/invoice'), // Redirect URL on Success.
        'cancel_url' => env('PG_PAYFAST_CANCEL_URL', 'http://e4d6c515.ngrok.io/invoice'), // Redirect URL on Cancellation.
        'notify_url' => env('PG_PAYFAST_NOTIFY_URL', 'http://e4d6c515.ngrok.io/payment/process') // ITN URL.
    ],

    'passphrase' => env('PG_PAYFAST_PASSPHRASE', 'sand3bOxPass'),

    'hosts' => [
        'www.payfast.co.za',
        'sandbox.payfast.co.za',
        'w1w.payfast.co.za',
        'w2w.payfast.co.za',
    ],

];